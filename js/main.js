$(document).ready(function () {

$("#vamosButton").hover(

    function() {
        $( this ).attr("src", "images/botones_hover.png" );
      }, function() {
        $( this ).attr("src", "images/botones.png" );
      }

);

$("#trabajosButton").hover(

    function() {
        $( this ).css("box-shadow", "-1px 3px 57px 3px rgba(230,215,0,1)")
      }, function() {
        $( this ).css("box-shadow", "none");
      }

);

$("#twitter").hover(

    function() {
        $( this ).attr("src", "images/twitter_hover.png" );
      }, function() {
        $( this ).attr("src", "images/twitter.png" );
      }
);

$("#googleplus").hover(

    function() {
        $( this ).attr("src", "images/gplus_hover.png" );
      }, function() {
        $( this ).attr("src", "images/gplus.png" );
      }

);

});